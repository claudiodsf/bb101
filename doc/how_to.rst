.. Getting started with BackTrackBB

======
How to
======

This is a quick guide for installing and running the detection and location code ``BackProj.py``

**System requirements** : 

* python 2.7.x (< 3.0)
* NumPy
* SciPy
* Matplotlib
* ObsPy, available at: http://obspy.org
* NonLinLoc - optional for time grid calculation (available here: http://alomax.free.fr/nlloc/). Otherwise can use any other way for calculating theoretical travel times. 

**Accepted data formats** : basic formats acceptable by ObsPy

Installation
============

Install in the usual Python manner : ::

  python setup.py install

In case you want to use ``NonLinLoc`` to calculate the 3D time-grids, download it from Anthony Lomax's website http://alomax.free.fr/nlloc/. You only need to be able to call ``Vel2Grid`` and ``Grid2Time`` correctly.

Running detection and location code
===================================

To run the main detection and location code execute: ::
    
    $ python BackProj.py congig_file.conf

You need to provide a configuration file containing the description of parameters required by the code.

Config file and parameters
==========================

Below are the basic parameters you should specify for the code: 

**ncpu** - is the number of CPUs that will be used for running the code
 - *ncpu = 1* corresponds to parallel version (useful for debugging)
 - *ncpu > 1* will run parallel version of the code in which detection and location for each position of sliding window will be performed as different process

**stations** - list of station names to be used for location. Should be separated by ``,`` and be the same as in the header of the data file 

**channel** - name of the channel component (EW,NS,UD). Should use the same as in the header of the data file

**data_dir** - path to the directory containing the data

**wave_type** - type of the phase assume for location (P/S)

**data_type** - data format ('SAC', 'mseed','gse2',...)
    
**data_dir** - path to the directory containing the data   

**grid_dir** - path to the directory containing the theoretical travel-time grids for corresponding phase assumption

**out_dir** - path to the directory where the outputs of the run will be saved (will be created by the code if does not exist)

**sample_rate_data** - sampling rate of the analyzed data. All the data will be resampled to this sampling rate before the analysis starts

**sample_rate_cf** - sampling rate of the CFs. All the CFs will be resampled to this sampling rate (using lower sampling rate for CF can be useful for reducing the calculation times)

**decay_const** - value in seconds of decay constant :math:`T_{decay}` that will be used for calculatin CF (kurtosis/envelope)

**ch_function** - type of CF. Options available are `kurtosis` (HOS :math:`2^{nd}` order) or `envelope`

**win_type**:
 - if set to ``False`` a constant :math:`T_{decay}` = decay_const will be used for time-frequency CF calculation
 - if set to ``True`` frequency-dependent :math:`T_{decay}` will be used

**f_min** - lower frequency :math:`f_{min}` of the multi-band filter (MBF)

**f_max** - lower frequency :math:`f_{min}` of the multi-band filter (MBF)

**n_freq_bands** - number of frequency bands :math:`N_{band}` between **f_min** and **f_max** for the filter-bank
    
**band_spacing** - choose between liniarly (``lin``) and logarithmically (``log``) spaces frequency bands for MBF filter 

**time_lag** - (in seconds) defines at the same time value of maximum lag in  local cross-correlation, and size of sliding time-window. Should be set to the maximum possible travel-time between the two furtherst stations 

**maxSTA_distance** - maximum distance (in km) between the pair of stations that will be used for detection and location

**t_overlap** - overlap (in seconds) between the two consecutive time windows 

**start_t** - position of the first time-window (in seconds) from the begining of the record

**end_t** - position of the last time-window (in seconds) from the begining of the record

**dt_min** - maximum time difference (in seconds) between the picked and theoretical travel-time used for calculating the origin time

**do_smooth_lcc** - will run a 2D Gaussian smoothing over the LCC function

**smooth_lcc** - value (in seconds) for Gaussian time-findow smoothing for LCC                            

**cut_data** - set True if want to cut a smaller length of data for analysis

**cut_start** - cutting start time (in seconds) from the begining of the record                     

**cut_delta** - length (in seconds) of data to cut

**trigger** - set value of detection threshold

**lat_orig** - latitude of the origin of XYZ grid used for detection and location

**lon_orig** - latitude of the origin of XYZ grid used for detection and location

**plot_waveforms** - option for plotting the seismograms on the final plot (``True``/``False``)

**plot_format** - select output format for the plotting (png/pdf)

|

An example configuration file ``BT_ChileExample.conf``::

    #--number of CPU's-------------------------------------------------------
    ncpu = 1    
    #--station names---------------------------------------------------------
    stations = QF17,G07S,QF05,G02S,QC12,QF16,G03S,QF12,G11S,G06S,U67B,U51B,U45B,G08S,U66B,U65B,G09S,U46B,QF14,G12S,U56B,G13S
    channel = HHZ
    wave_type = 'P'
    #--settings for input data files-----------------------------------------
    data_type = 'mseed'
    data_dir = 'examples/data/data_Chile'
    grid_dir = 'examples/grids/grids_Chile'
    #
    out_dir = 'out_example_Chile/'
    #-----------------------------------------------------------------------
    sampl_rate_data = 100.                      
    sampl_rate_cf = 50.                         
    #------------------------------------------------------------------------
    decay_const = 1.00                          
    ch_function = 'kurtosis'
    win_type = False
    #--Parameters for the MBF analysis--------------------------------------
    f_min = 0.02
    f_max = 49.
    n_freq_bands = 20
    band_spacing = log
    #------------------------------------------------------------------------
    time_lag = 20.                              
    maxSTA_distance = 100.                      
    #--Parameters for the sliding window------------------------------------
    t_overlap = 17.                            
    start_t = 33.
    end_t = 35.
    #
    dt_min = 1.0
    #---LCC calculatioin settings-------------------------------------------
    do_smooth_lcc = False
    smooth_lcc = 0.1                            
    #------------------------------------------------------------------------
    cut_data = False
    cut_start = 0.                     
    cut_delta = 180.                  
    #------------------------------------------------------------------------
    trigger = 0.7
    #--geogr. coordinates for the origin of the coordinate system-----------
    lat_orig = -35.404
    lon_orig = -73.000
    #--parameters for plotting----------------------------------------------
    plot_waveforms = True
    plot_format = png


Outputs
=======

Run the example provided with the code: ::
    
    $ python BackProj.py BT_ChileExample.conf
    
    
    use of var time window for location: False
    Number of traces in stream =  22
    Number of time windows =  1
    frequencies for filtering in (Hz): [  2.00000000e-02   3.01583209e-02   4.54762160e-02   6.85743157e-02
    1.03404311e-01   1.55925020e-01   2.35121839e-01   3.54543993e-01
    5.34622576e-01   8.06165961e-01   1.21563059e+00   1.83306887e+00
    2.76411396e+00   4.16805178e+00   6.28507216e+00   9.47736116e+00
    1.42910650e+01   2.15497261e+01   3.24951778e+01   4.90000000e+01]
    starting BPmodule
    Running on 1 thread
    22
    20100401_1253A X 5.0 Y 33.0 Z 16.0 MaxStack 0.858 Ntraces 22 BEG 33.0 END 53.0  LAT -34.70242 LON -72.04541 T_ORIG 2010-04-01T12:53:05.343609Z

Since in the configuration file ``BT_ChileExample.conf`` ouput directory is set to ``'out_example_Chile/'`` all the outputs will be redirected there ::
    
    $ ls out_example_Chile/

    10040112_20fq0.0_49.0hz_1.050.00.117.0_kurtosis_HHZ_P_trig0.7_FIG2.png
    0040112_20fq0.0_49.0hz_1.050.00.117.0_kurtosis_HHZ_P_trig0.7_OUT2.dat
    10040112_t0033.0s_0.0_49.0_fig.png

where 
    - ``*_FIG2.png`` is a summary plot of triggered locations 
    - ``*_fig.png`` format for the plots for each position of the sliding time-window
    - ``*_OUT2.dat`` summary data file providing the information on the triggered locations and the picked and theoretical arrival times from this location for each station



