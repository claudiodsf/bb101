.. _Method:

######
Method
######

Overview
========
``BackTrackBB.py`` 

Signal Processing - Broadband Characteristic Function Calculation
=================================================================

Exctracting non-stationary frequency-dependent statistical features of the recorded seismic signals.

|

Higher-Order Statistics (HOS) CFs
---------------------------------

Estimating CF of the raw record by extracting information on the statistical variations of the signal. Suitable for enhansing and extracting **short transient signals** e.g., local earthquakes, phase arrivals from distant events, and other short transients assosiated to low frequency earthquakes and vulcanic earthquakes.
HOS perform well in the conditions of low signal-to-noise ratio.

*HOS of a probability distribution for a random variable* :math:`X` *are defined by the standardized moment:*

.. math::

   S_n = \frac
              {E \left[ \left( X - \mu \right)^n \right]}
              {\left( E \left[ \left( X - \mu \right)^2 \right] \right)^ {n/2}}
              = \frac{m_n}{m_2^{n/2}}

where :math:`E\left[\cdot\right]` is the expectation operator, :math:`\mu = E\left[X\right]` is the mean,
:math:`m_2 = \sigma^2` is the variance (second moment), and :math:`m_n` is the 
:math:`n^{th}` central moment.  

**Kurtosis**, is the HOS of the :math:`4^{th}` order. It is defined as a :math:`4^{th}` moment about the mean 
normalized by the square of the :math:`2^{nd}` moment (variance) - :math:`S_4 = m_4/m_2^2` . Although, HOS of higher orders can be equally used, in his version of the program we limit the calculation of HOS CFs to kurtosis. 

Calculation of kurtosis for a discrete signal :math:`u\left(t\right)` (seismic record) is clasically performed in a sliding window. This can be computationally expensive. To improve the computational performance we implemented a recursive scheme based on formulation of the exponential moving average. Such scheme provides an efficient way for accumulating time-averaged statistics. The recursive formula for exponential moving average :math:`\hat{\mu}\left(t\right)` of signal :math:`u\left(t\right)` is defined as:

.. math::
    \hat{\mu}\left(t_i\right)=
                            C\times
                            u\left(t_i\right)+\left(1-C\right) \times
                            \hat{\mu}\left(t_{i-1}\right)

where :math:`u\left(t_i\right)` is the :math:`i`-th sample of the signal, :math:`\hat{\mu}\left(t_{i-1}\right)` previous 
estimate of the average, and :math:`C` is the decay constant (:math:`0\leq 1-C \leq 1`).
Directly extending the above definition to the estimates of the :math:`2^{nd}` and :math:`4^{th}` cantral moments, and using the definition of HOS we obtain the following recursive expression for the kurtosis CF:

.. math::
    CF_{HOS} \left(t_i\right) \equiv\ \hat{S}_4\left(t_i\right) = \frac
    {C \times \left[u\left(t_i\right) -  \hat{\mu}\left(t_{i-1} \right)\right]^4
    +\left( 1-C \right) \times  \hat{m}_4\left(t_{i-1}\right)}
    {\left( C \times \left[u\left(t_i\right) -  \hat{\mu}\left(t_{i-1} \right)\right]^2
            +\left( 1-C \right) \times  \hat{m}_2\left(t_{i-1}\right)\right)^2}
            
An example of the recursive kurtosis CF calculated on a seismogram of a reginal earthquake is shown below. 

.. image:: figures/HOS.png
  :width: 500px
  :align: center

The decay constant :math:`C` can be as seen as a smoothing factor, defining how fast the memory of older data-points will be discarded. It is expressed as :math:`C = \frac{\Delta T}{T_{decay}}` where :math:`\Delta T` is sampling rate of the data and :math:`T_{decay}` represents the time-length of the averaging scale. Effect of different values of :math:`C` constant on the final CF is shown in the following figure.

.. image:: figures/HOS2.png
  :width: 500px
  :align: center

Decay constant :math:`C` should be set separately for each particular case study, through a preliminary testing step. The actual value of this parameter is defined by selecting :math:`T_{decay}` (in seconds) in the program's contral file. The parameter is further transformed into the decay constant, using the sampling rate of the data.

|

Envelope CFs
------------

Characterization of signal based on the amplitude or energy modulation - *energy envelope* that can be applied to signals associated to sources characterized byslow energy transients, lasting several tens of seconds e.g., *tectonic tremors*.The relative timing of localized energy transients can be used to detect and locate a tremor source.

The root-mean-square (RMS) envelope CF function is defined as:

.. math::

   CF_{env} \left( t_i \right) \equiv
                                RMS(t_i) =
                                           \sqrt {\frac{\sideset{}{_{i=1}^M}\sum u(t_i)^2}{M-1}}

Following the same idea of recursive implementation, we define the recursive envelope CF as a single component recursive RMS envelope of the recorded 
signal :math:`u\left(t_i\right)` using following expression:

.. math::
   CF_{env}( t_i ) \equiv 
                         \widehat{RMS}(t_i) = \sqrt{C \times u^2(t_i) + (1-C) \times\left[\widehat{RMS}(t_{i-1}) \right]^2 }

|

.. image:: figures/env.png
  :width: 500px
  :align: center

Similarly to the recursive kurtosis CF, parameter to be set for the calculation is :math:`T_{decay}` in seconds, from which the decay constant is calculated as 
:math:`C = \frac{\Delta T}{T_{decay}}`. 

|

Time-Frequency Analyasis: Multi-Band Filter
-------------------------------------------

The Multi-Band Filter (MBF) algorithm provides a time-frequency decomposition of the signal by producing a set of band-pass filtered time series 
with different central periods. The emplemented MBF scheme follows the recursive multi-band filtering scheme of `FilterPicker <http://alomax.free.fr/FilterPicker/>`_ algorithm by Lomax et al. (2012). MBF analysis is performed by running the original record through a predefined bank of narrow band-pass filters covering the interval :math:`\left[ f_{min},f_{max} \right]`, and having :math:`n = \left\{0,...,N_{band}\right\}` central frequencies (either linearly or logarithmically spaced). On practice, filtering is implemented through a cascade of two simple low-pass and high-pass, one-pole recursive filters. This is equivalent to a two-pole band-pass filter. Below is an illustration of such a filter-bank:    

.. image:: figures/MBF_filter.png
  :width: 250px
  :align: center

A frequency-dependent CFs (:math:`CF(t,f_n)`) calculated by running the kurtosis/envelope recursive estimations for each of the filtered signals. This yields a time-frequency dependent representation of the signal's characteristics.

.. math::
    CF^{TF}(t)=\max\limits_f CF(t,f) & \quad f\in [f_{min},f_{max}]

.. image:: figures/MBF_example2.png
  :width: 500px
  :align: center

|

**Generalized Characteristic Function**

.. math::
    CF(t) = 
    \begin {cases}
    \sideset{}{_{HOS}^+}{\dot{CF}}(t) \ast e^{t^2/4\sigma^2}
                                     & \quad \text{if } CF\text{_}type \text{ is } kurtosis \\
     \\
    CF_{env}(t) & \quad \text{if } CF\text{_}type \text{ is } envelope \\
    \end{cases}

|

.. image:: figures/generic_CF.png
  :width: 500px
  :align: center

|
|
| 


Detection and Location Scheme
=============================

blabla

Stations-pair Time Delay Estimate (TDE) Functions
-------------------------------------------------


.. image:: figures/LCC.png
  :width: 500px
  :align: center

|

Space-Time Imaging
------------------

.. image:: figures/SpaceTimeImaging.png
  :width: 600px
  :align: center


|

Detection and Location
----------------------

.. image:: figures/DetectionLocation.png
  :width: 500px
  :align: center

**Analysing continuous data**


