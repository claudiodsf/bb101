.. ll documentation master file, created by
   sphinx-quickstart on Tue Jan 19 10:01:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Multi-band Detection and location code documentation
====================================================

Contents:

.. toctree::
   :maxdepth: 2

   Method
   how_to
   tests_examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

