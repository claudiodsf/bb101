## This is repository for BackTrackBB detection and locaton code

(c) 2015-2016  Natalia Poiata <poiata@ipgp.fr>, Claudio Satriano <satriano@ipgp.fr>; (c) 2013-2014  Natalia Poiata <poiata@ipgp.fr>, Claudio Satriano <satriano@ipgp.fr>, Pierre Romanet <romanet@ipgp.fr>  



###To install run:

    python setup.py install


## Sample runs:
First, download the file [examples.zip]( https://www.dropbox.com/s/emlz4lbd6dpu9a9/examples.zip?dl=0) containing additional data (seismograms and theoretical travel-time grids).


### Run the main detection and location code on an example dataset:

    ./BackProj.py  BT_ChileExample.conf

### Run an example illustrating the procedure of Multi-Band Filter Characteristic Function calculation: 

    ./MBF_plot.py  MBF_ChileExample.conf



### A detailed documentation is available here: [link_to_doc_repository] 


